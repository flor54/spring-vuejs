package com.example.demo.model;

public class Friend {
	int id;
	String nombre;
	String username;
	
	public Friend(int id,String nom, String username) {
		this.id = id;
		this.nombre = nom;
		this.username = username;
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
