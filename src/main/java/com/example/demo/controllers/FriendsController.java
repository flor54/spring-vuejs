package com.example.demo.controllers;



import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Friend;


//@CrossOrigin(origins = "*", maxAge = 3600)
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/friends")
public class FriendsController {
	
	private List<Friend> friends;
	
	public FriendsController() {
		this.friends = new ArrayList<Friend>();
		friends.add(new Friend(1,"flor","flor54"));
		friends.add(new Friend(2,"alex","alf"));
		friends.add(new Friend(3,"martu","martuEstrella"));
		friends.add(new Friend(4,"mia","AmoreMia"));
		friends.add(new Friend(5,"mello","mello123"));
		friends.add(new Friend(6,"near","nearDN"));
		friends.add(new Friend(7,"misa","misaAmane"));
	}
	
	
	
	@GetMapping("")
	public List<Friend> listFriends() {
		return friends;
	}	
	
	
	@GetMapping("/{id}")
	public Friend get(@PathVariable int id) {
		return friends.stream().filter(f -> id == f.getId()).findAny().orElse(null);
	}	

}
