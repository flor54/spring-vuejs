package com.example.demo.controllers;



import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.demo.model.Friend;

@Controller
@RequestMapping("/")
public class ExampleController {
	
	@GetMapping("")
	public String index(Model model) {
		model.addAttribute("title", "Hello desde Spring Boot");
		return "list";
	}
	
	@GetMapping("/{id}")
	public String get(Model model,@PathVariable int id) {
		model.addAttribute("id", id);
		return "detail";
	}	


}
